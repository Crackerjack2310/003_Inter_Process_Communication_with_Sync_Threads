#include"header.h"
#include"shm_com.h"

void main()
{
	int shmid, running = 1;
	shm *shared;
	shmid = shmget((key_t) 12, sizeof(shm), 0666 | IPC_CREAT);
	if(shmid == -1)
	{
		perror("shmid()");
		exit(EXIT_FAILURE);
	}

	shared = shmat(shmid, (void*)0, 0);	// attaches to the suitable shared memory segment and returns it
	if(shared == (void*)-1)
	{
		perror("shmid()");
		exit(EXIT_FAILURE);
	
	}
	printf("Memory attached at %x shmid : %d\n",(int)shared, shmid);
	
	shared->info = 0;

	printf("entering loop\n");
	while(running)
	{
		if (shared->info)
		{
			printf("You wrote : %s", shared->data);
			sleep(1);

			shared->info = 0;

			if (strncmp(shared->data,"end",3) == 0)
				running = 0;
		}
	
	}
	
	if(shmdt(shared) == -1)
	{	perror("shmdt()");
		exit(EXIT_FAILURE);

	}
	
	if(shmctl(shmid, IPC_RMID, 0) == -1)
	{	perror("shmctl()");
		exit(EXIT_FAILURE);

	}
	exit(EXIT_SUCCESS);
}
