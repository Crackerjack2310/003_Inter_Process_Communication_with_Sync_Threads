typedef struct request
{
	char oper;
	int opr1;
	int opr2;
	int c_flag;
	pid_t pid;
}request;

typedef struct receive
{
	int s_flag;
	int result;
	pid_t pid;
}receive;


////////////////////////////////////////////////////////////   for semaphores


union semun {
               int              val;    /* Value for SETVAL */
               struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
               unsigned short  *array;  /* Array for GETALL, SETALL */
               struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                           (Linux-specific) */
            };
