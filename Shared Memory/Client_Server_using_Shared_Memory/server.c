#include"header.h"
#include"structures.h"
#include"declarations.h"


int main() 		// server begins
{
	signal(SIGINT,sig_handler); 		// for deleting the semaphores
	printf("\n%s : Begin \n",__func__); 
	int sem_id, clients = 1, count, fd1, fd2, result;
	
	pid_t ret;
	request r;
	receive *res, res_value;
	system("clear");

	while(1)		// infinite loop for server
	{
	//	printf("Client PID : %d\n",res.pid);
	//	printf("result from server : %d\n",res.result);
	//	printf("server waiting for client : %d to open for receive \n",res.pid);
		
		res_value = req_processing();
       	 	int shmid = shmget((key_t) 1234, BUFSIZ, 0666 | IPC_CREAT);
       		if (shmid == -1)
        	{
	                perror("shmget()");
                	exit(EXIT_FAILURE);
        	}

		res = (receive*)shmat(shmid, NULL, 0);
		res = res + sizeof(request);

		res->result = res_value.result;
		res->s_flag = 0;

		printf("result : %d\n",res->result);
		printf("\nServer wrote to the requesting client no : %d\n\v",clients);
		printf("\v############################################################\v\n");
		clients++;

	}
	printf("\n%s : End \n",__func__);
	return 0;
}

void sig_handler(int sig)  // as we need SIGINT to terminate server as of now. hence it deletes the semaphore as well.
{
       	int shmid = shmget((key_t) 1234, BUFSIZ, 0666 | IPC_CREAT);
       	if(shmctl(shmid, IPC_RMID, 0) == -1)
        {
	 	perror("shmctl()");
                exit(EXIT_FAILURE);
	}
	printf("Shared memory released successfully\n");
	exit(EXIT_SUCCESS);

}
