#include"header.h"
#include"structures.h"
#include"declarations.h"
#include"sem_functions.h"

void main()
{
	 if(!sem_v(1))   /*Semaphore 1 released as value restored to sem_value, now available
                        for the server willing to write to the FIFO client_receive*/
                exit(EXIT_FAILURE);
}
