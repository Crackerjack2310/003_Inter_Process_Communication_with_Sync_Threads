#include"header.h"
#include"structures.h"
#include"declarations.h"
#include"sem_functions.h"

int check = 1;

int main() 	// client1 
{
	printf("\n%s : Begin \n",__FILE__); 
	signal(SIGINT, sig_handler);
	int sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT); 
	if(sem_id == -1) // setting semaphore value for using it the first time.
        {
                fprintf(stderr,"Failed to get sem_id\n");// initializing semaphore, putting semval = val
                exit(EXIT_FAILURE);
        }	
	
	if(!sem_p(0))  // got the 1st semaphore here the process will get stuck if the semaphore value, i.e. value for semaphore is -1 (already decremented by someother process) otherwise the process will get the semaphore value and proceed, setting the value as -1.
       		 exit(EXIT_FAILURE);


//************************************************************************************* Critical 1  section begins
	

	printf("%s : PID : %d got semaphore 1\n",__FILE__,getpid());
	
	int fd1, fd2, count, ret, result;
	request r;
	r.oper = '-';
	r.opr1 = 10 ;
	r.opr2 = 5 ;
	r.pid = getpid();

	printf("%s : PID : %d --->> operator : %c operand1 : %d  operand2 : %d with PID : %d\n",__FILE__,getpid(),r.oper,r.opr1,r.opr2,r.pid);
        
	printf("%s : PID : %d Client_send is blocked \n",__FILE__,getpid()); 
	fd1 = open("client_send",O_WRONLY);	// to write to the server 
	if(fd1 == -1)
	{
		perror("openWR()");
		exit(EXIT_FAILURE);
	}	
	printf("%s : PID : %d Client_send opened \n",__FILE__,getpid()); 
	count = write(fd1,&r,sizeof(r));	// write the structure to the server
	printf("%s : PID : %d wrote %d bytes \n",__FILE__,getpid(),count); 
	close(fd1);

	printf("%s : PID : %d Client_receive blocked\n",__FILE__,getpid());
	while(check); 					// loop until signal = 0;
	printf("%s : PID : %d Loop broken\n",__FILE__,getpid());

	fd2 = open("client_receive",O_RDONLY);	// to write to the server
	printf("%s : PID : %d Client_receive opened\n",__FILE__,getpid());
	if(fd2 == -1)
	{
		perror("openRD()");
		exit(EXIT_FAILURE);
	}	

	

/************************************************************************************* Critical 1 section ends
	
	printf("%s : PID : %d Now releasing semaphore 1\n"__FILE__,getpid());

	if(!sem_v(0)) // 1st  semaphore released
        	exit(EXIT_FAILURE);     // semaphore value has been restored to 1, now available for waiting processes.
					// Moved to req_processing as of now for testing
	
//	printf("%s : PID : %d released semaphore 1\n"__FILE__,getpid());
	

	if(!sem_p(1)) // got 2nd semaphore....here the process will get stuck if the semaphore value, i.e. value for semaphore is -1 (already decremented by someother process) otherwise the process will get the semaphore value and proceed, setting the value as -1.
                 exit(EXIT_FAILURE);


************************************************************************************* Critical 2 section begins

*/	printf("%s : PID : %d got semaphore 2\n",__FILE__,getpid());
	printf("%s : PID : %d block on read\n",__FILE__,getpid());

	count = read(fd2,&result,sizeof(result));	// read the structure from server 
        close(fd2);
	printf("%s : PID : %d read %d bytes from client_receive\n",__FILE__,getpid(),count);
	printf("%s : PID : %d got result : %d\n",__FILE__,getpid(),result);
	
//************************************************************************************* Critical 2 section ends

	printf("%s : PID : %d Now releasing semaphore 1\n"__FILE__,getpid());
	if(!sem_v(0)) // 2nd semaphore released
        	exit(EXIT_FAILURE);     // semaphore value has been restored to 1, now available for waiting processes.
					// Moved to req_processing as of now for testing

	printf("%s : PID : %d released semaphore 1\n",__FILE__,getpid());
	printf("\n%s : PID : %d Ended \n",__FILE__,getpid());

	return 0;
}
void sig_handler(int sig)
{
	check = 0;
}

