
union semun {
               int              val;    /* Value for SETVAL */
               struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
               unsigned short  *array;  /* Array for GETALL, SETALL */
               struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                           (Linux-specific) */
           };


static int set_semvalue(void)
{
        union semun sem_union;
        sem_union.val = 1;
        if(semctl(sem_id, 0, SETVAL, sem_union) == -1)
	{
         	printf("sem_id : %d\n",sem_id);
	 	return 0;
	}
		return 1;
}


//////////////////////////////////////////////////////// 5 ////////////////////////


static void del_semvalue(void)
{
        union semun sem_union;
        sem_union.val = 1;
        if(semctl(sem_id, 0, IPC_RMID, sem_union) == -1)
	{
        	fprintf(stderr,"Failed to delete semaphore");
		exit(EXIT_FAILURE);
	}
}


////////////////////////////////////////////////// 6 ///////////////////////////////

static int semaphore_p(void)
{
        struct sembuf sem_b;
        sem_b.sem_num  = 0;
        sem_b.sem_op  = -1;
        sem_b.sem_flg  = SEM_UNDO;

        if(semop(sem_id, &sem_b, 1) == -1)
        {
                fprintf(stderr,"semaphore_p failed\n");
                return 0;
        }
        return 1;
}

static int semaphore_v(void)
{
        struct sembuf sem_b;
        sem_b.sem_num  = 0;
        sem_b.sem_op  = 1;
        sem_b.sem_flg  = SEM_UNDO;

        if(semop(sem_id, &sem_b, 1) == -1)
        {
                fprintf(stderr,"semaphore_v failed\n");
                return 0;
        }
        return 1;
}

 
