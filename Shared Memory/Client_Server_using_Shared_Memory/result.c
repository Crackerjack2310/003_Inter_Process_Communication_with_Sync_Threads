#include"header.h"
#include"request.h"
#include"declarations.h"

int main(int argc, char *argv[]) 	// result
{
	printf("\n%s : Begin -- result --\n",__FILE__); 

	int rfd, count;
	request r;

	rfd = atoi(argv[1]);	//write file descriptor of pipe 1
	count = read(rfd,&r,sizeof(request));	// write the structure to the pipe 1
	
	printf("\n%s read %d bytes from pipe 3\n",__FILE__,count); 
	printf("value after processing -->>\n r.result : %d \n",r.result);
	printf("\n%s : End -- result --\n",__FILE__);
	
	return 0;
}
