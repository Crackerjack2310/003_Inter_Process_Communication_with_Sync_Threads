#include"header.h"
#include"structures.h"
#include"declarations.h"

int main() // subtractor
{
	printf("\n%s : Begin \n",__FILE__); 

	int result, count, fd3, fd4;
	request r;

	fd3 = open("proc_read",O_RDONLY);
	count = read(fd3,&r,sizeof(r)); // reading the structure from proc_read
	close(fd3);
	printf("value read by %s : %d bytes\n",__FILE__,count);
	result = r.opr1 - r.opr2; // adding and writing value to result
	printf("result : %d\n",result);
	fd4 = open("proc_write",O_WRONLY);
	count = write(fd4,&result,sizeof(result)); // writing the calculated value into proc_write
	close(fd4);
	printf("value written by %s : %d bytes\n",__FILE__,count);
	printf("\n%s : End \n",__FILE__);
		
	return 0;
}
