#include"header.h"
#include"structures.h"
#include"declarations.h"

receive req_processing()
{
	printf("\n%s : Begin \n",__func__); 

	request *req;
	receive res;
	
	int count, opr1, opr2, result, fd3, ptr, fd4, flag = 1, fd1;
	int shmid;
        shmid = shmget((key_t) 1234, BUFSIZ, 0666 | IPC_CREAT);
        if (shmid == -1)
	{
		perror("shmget()");
		exit(EXIT_FAILURE);
	}
	req = (request*)shmat(shmid, NULL, 0);

	req->c_flag = 1;
	while(req->c_flag);	

	opr1 = req->opr1;
	opr2 = req->opr2;

	pid_t ret;
	ret = fork();   // child process for req_processing for processing clients

        switch(ret)
	{        
                case -1:                         
                        perror("fork()");
                        exit(EXIT_FAILURE);
                case 0:                                 // child
			switch(req->oper)
			{
				case '+': execl("./adder","adder", NULL);        
				case '-': execl("./subtractor","subtractor", NULL);        
				case '*': execl("./multiplier","multipier", NULL);        
				case '/': execl("./divider","divider", NULL);        
				default:
					printf("Invalid operator choice..Going on block\n");
			}
			break;
                default:                                // parent 
                        
			printf("This is req_processing unit\n");
			req = req + (sizeof(request) + sizeof(receive) + sizeof(request));
					
			req->c_flag = 1;
			req->opr1 = opr1;
			req->opr2 = opr2;
			req->c_flag = 0;
	
       			waitpid(ret,&ptr,0);	
			printf("%s wrote to processing client \n",__func__);
			
			req = (request*)shmat(shmid, NULL, 0);
			req = req + (sizeof(request) + sizeof(receive) + sizeof(request));
      			req->c_flag = 1;
			res.result = req->opr1;
			printf("%s read from the requesting client\n",__func__);
	}	

	printf("%s says client PID : %d\n",__func__,res.pid);
	printf("\n%s : End \n",__func__);
	
	return res;
	
}
