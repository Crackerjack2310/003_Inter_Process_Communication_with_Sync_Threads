#include"header.h"
#include"structures.h"
#include"declarations.h"

int main() 		// client1 
{
	printf("\n%s : PID : %d Begin \n",__FILE__,getpid()); 

	receive *res;
	request *req;

///////////////////////////////////////////////////////////////// write result

	int shmid;
	shmid = shmget((key_t) 1234, BUFSIZ, 0666 | IPC_CREAT);
	req = (request*)shmat(shmid, NULL, 0);
	
	req->oper = '+';
	req->opr1 = 100 ;
	req->opr2 = 5 ;
	req->pid = getpid();
	req->c_flag = 0;

//	printf("%s : PID : %d --->> operator : %c operand1 : %d  operand2 : %d with PID : %d\n",__FILE__,getpid(),r.oper,r.opr1,r.opr2,r.pid);
	
//////////////////////////////////////////////////////////////////// read result
	
        res = (receive*)shmat(shmid, NULL, 0);
	res = res + sizeof(request);
	res->s_flag = 1;
	while(res->s_flag);
	
	printf("%s : PID : %d got result : %d\n",__FILE__, getpid(), res->result);
	printf("\n%s : PID : %d Ended \n",__FILE__, getpid());
	
	return 0;
}
