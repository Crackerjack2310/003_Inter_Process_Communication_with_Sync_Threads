#include"header.h"
#include"shm_com.h"

void main()
{
	int shmid, running = 1;
	void *shared_memory = (void*)0;
	struct shmid_ds *buf;
	shm *shared;
	shmid = shmget((key_t) 12, sizeof(shm), 0666 | IPC_CREAT);
	if(shmid == -1)
	{
		perror("shmid()");
		exit(EXIT_FAILURE);
	}
	shared_memory = shmat(shmid, (void*)0, 0);
	if(shared_memory == (void*)-1)
	{
		perror("shmid()");
		exit(EXIT_FAILURE);
	}
	printf("Memory attached at %x shmid : %d\n",(int)shared_memory, shmid);	
	shared = (shm*)shared_memory;
	char buffer[BUFSIZ];
	shared->info = 0;

	while(running)
	{	
		while(shared->info == 1)
		{
			sleep(1);
			printf("Waiting for client........\n");	
		}	
		printf("Enter some text\n");
		fgets(buffer, BUFSIZ, stdin);
		strncpy(shared->data, buffer, 50);
		shared->info = 1;		
		if (strncmp(shared->data,"end",3) == 0)
			running = 0;
	}
	if(shmdt(shared_memory) == -1)
	{
		perror("shmdt()");
		exit(EXIT_FAILURE);
	}
		
	exit(EXIT_SUCCESS);
}
