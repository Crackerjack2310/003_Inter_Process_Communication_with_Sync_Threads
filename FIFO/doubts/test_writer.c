#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

void main()
{

	int ret,wfd,count;
	if(access("myfifo",F_OK) == -1)
	{
		ret = mkfifo("myfifo",0666);	// rw permissions for all probable users
	
		if(ret == -1)
		{
			perror("mkfifo");
			exit(EXIT_FAILURE);	
		}
		printf("fifo created successfully !!\n");
	}
	else
		printf("fifo already present \n");
	
	sleep(2);
	system("clear");
	printf("Awaiting receiver !! \n");
	
	wfd = open("myfifo",O_WRONLY);
	system("clear");
	
	printf("\nReceiver active..Ready to send !! \n\v");
	
	if(wfd == -1)
	{
		perror("open()");
		exit(EXIT_FAILURE);	
	}
	char *buff = "This was first --- and now u will be able to understand how FIFOs work, say thanks!!";
	int i = 1;
	while(i)
	{	
	//	printf("Sender -->>  ");
	//	fgets(buff,50,stdin);
	//
		count = write(wfd,buff,(int)strlen(buff));
		printf(" i = %d  Wrote : %d as %s \n", i,count,buff);
		i++;
	}

	close(wfd);

}
