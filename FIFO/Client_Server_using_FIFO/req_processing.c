#include"header.h"
#include"structures.h"
#include"declarations.h"

receive req_processing()
{
	printf("\n%s : Begin \n",__func__); 
        int sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT);
        
	printf("%s : PID : %d says : before client joins sem0 val  : %d\n", __FILE__, getpid(), get_semvalue(0));

	request r;
	receive res;
	int count, result, fd3, ptr, fd4, flag = 1, fd1;
	pid_t ret;
	printf("client_send is blocked as of now \n");
		
	fd1 = open("client_send",O_RDONLY);	//client FIFO open for reading
	printf("client_send opened for reading \n");
	count = read(fd1,&r,sizeof(r));		// Block on READ
	close(fd1);
        printf("%s says : Now releasing semaphore 0 for PID : %d \n", __FILE__, r.pid);


// *******************************      CRITICAL SECTION 0 ENDS       ****************************************


        printf("%s : PID : %d says : semvalue before v(0): %d\n", __FILE__, r.pid, get_semvalue(0) );

	if(!sem_v(0))   /*Semaphore 0 released as value restored to sem_value, now available
                        for any waiting clients willing to write to the FIFO client_send*/
        	exit(EXIT_FAILURE);
        
	printf("%s : PID : %d says : semvalue after v(0): %d\n", __FILE__, r.pid, get_semvalue(0));
        
	printf("%s : PID : %d released semaphore 0\n", __FILE__, r.pid);
	printf("%s read %d bytes from client : %d\n", __func__, count, r.pid);

	ret = fork();   // child process for req_processing for processing clients

        switch(ret)
	{        
                case -1:                         
                        perror("fork()");
                        exit(EXIT_FAILURE);
                case 0:                                 // child
			switch(r.oper)
			{
				case '+': execl("./adder","adder", NULL);        
				case '-': execl("./subtractor","subtractor", NULL);        
				case '*': execl("./multiplier","multipier", NULL);        
				case '/': execl("./divider","divider", NULL);        
				default:
					printf("Invalid operator choice..Going on block\n");
			}
			break;
                default:                                // parent 
                        printf("This is req_processing unit\n");
			fd3 = open("proc_read",O_WRONLY);
			count = write(fd3,&r,sizeof(request));	// write the structure to proc_read 
			close(fd3);
			printf("%s wrote %d bytes \n",__func__,count);
			
			fd4 = open("proc_write",O_RDONLY);
			count = read(fd4,&result,sizeof(result));	// read the structure written to pipe3 by adder 
			close(fd4);
			printf("%s read %d bytes \n",__func__,count);
	}	

	res.result = result = 100;
	res.pid = r.pid;
	printf("%s says client PID : %d\n",__func__,res.pid);
	
        printf("%s : PID : %d says : check v(0): %d\n", __FILE__, r.pid, get_semvalue(0));

	printf("\n%s : End \n",__func__);
       	waitpid(ret,&ptr,0);
	return res;
	
}
