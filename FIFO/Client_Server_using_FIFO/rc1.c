#include"header.h"
#include"structures.h"
#include"declarations.h"

int main() 		// client1 
{
	printf("\n%s : PID : %d Begin \n",__FILE__,getpid()); 

	receive res;
	int sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT); 
	
	if(sem_id == -1) // setting semaphore value for using it the first time.
        {
                fprintf(stderr,"Failed to get sem_id\n");// initializing semaphore, putting semval = val
                exit(EXIT_FAILURE);
        }	
	
	printf("%s : PID : %d says : semvalue before p(0): %d\n", __FILE__, getpid(), get_semvalue(0));
	
	if(!sem_p(0))   /*Semaphore 1 released as value restored to sem_value, now available
                        for the server willing to write to the FIFO client_receive*/
                exit(EXIT_FAILURE);
	
	printf("%s : PID : %d says : semvalue after p(0): %d\n", __FILE__, getpid(), get_semvalue(0));

	printf("%s : PID : %d got semaphore 0\n",__FILE__,getpid());	
	int fd1, fd2, count, ret, result;
	request r;
	r.oper = '+';
	r.opr1 = 10 ;
	r.opr2 = 5 ;
	r.pid = getpid();

	printf("%s : PID : %d --->> operator : %c operand1 : %d  operand2 : %d with PID : %d\n",__FILE__,getpid(),r.oper,r.opr1,r.opr2,r.pid);
        printf("%s : PID : %d Client_send is blocked \n",__FILE__,getpid()); 
	
	fd1 = open("client_send",O_WRONLY);	// Block on OPEN
	if(fd1 == -1)
	{
		perror("openWR()");
		exit(EXIT_FAILURE);
	}	
	printf("%s : PID : %d Client_send opened \n", __FILE__, getpid()); 
	count = write(fd1,&r,sizeof(r));	// write the structure to the server
	close(fd1);
	printf("%s : PID : %d wrote %d bytes \n", __FILE__, getpid(), count); 

	printf("%s : PID : %d Client_receive blocked\n", __FILE__, getpid());
	
	fd2 = open("client_receive",O_RDONLY);	// Block on OPEN
	printf("%s : PID : %d Client_receive opened\n", __FILE__, getpid());
	if(fd2 == -1)
	{
		perror("openRD()");
		exit(EXIT_FAILURE);
	}	
	printf("%s : PID : %d block on read\n", __FILE__, getpid());

	count = read(fd2,&res,sizeof(res));	// Block on READ 
        close(fd2);

	printf("%s : PID : %d Now releasing semaphore 1\n", __FILE__, getpid());

	printf("%s : PID : %d says : semvalue before v(1): %d\n", __FILE__, getpid(), get_semvalue(1));
	
	if(!sem_v(1))   /*Semaphore 1 released as value restored to sem_value, now available
                        for the server willing to write to the FIFO client_receive*/
                exit(EXIT_FAILURE);
	
	printf("%s : PID : %d says : semval after v(1): %d\n", __FILE__, getpid(), get_semvalue(1));
	printf("%s : PID : %d released semaphore 1\n", __FILE__, getpid());
	printf("%s : PID : %d read %d bytes from client_receive\n", __FILE__, getpid(), count);
	printf("%s : PID : %d got result : %d\n",__FILE__, getpid(), res.result);
	printf("%s : PID : %d says : semval check : %d\n", __FILE__, getpid(), get_semvalue(0));
	printf("\n%s : PID : %d Ended \n",__FILE__, getpid());
	
	return 0;
}
