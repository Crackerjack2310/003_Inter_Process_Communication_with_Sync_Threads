#include"header.h"
#include"structures.h"

int set_semvalue(int sem_number)
{
	int sem_id;
	sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT);
        if(sem_id == -1) // setting semaphore value
	{
                fprintf(stderr,"Failed to get sem_id\n");	// initializing semaphore, putting semval = val
                exit(EXIT_FAILURE);
        }
	union semun sem_union;
	sem_union.val = 1;
        if(semctl(sem_id, sem_number, SETVAL, sem_union) == -1)
	{      
		printf("sem_id : %d",sem_id);
		perror("semctl()");
	      	return 0;
       
	}	return 1;
}

