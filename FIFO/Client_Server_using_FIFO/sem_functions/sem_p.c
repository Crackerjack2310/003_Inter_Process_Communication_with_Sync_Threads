#include"header.h"
#include"structures.h"

int sem_p(unsigned short sem_number)
{
	size_t nsops = 1;
     
	int sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT);
        if(sem_id == -1)
        {
                fprintf(stderr,"Failed to get sem_id\n"); // initializing semaphore, putting semval = val
                exit(EXIT_FAILURE);
        }
        struct sembuf sops;     // pointer to the array that has each element as struct sembuf
        sops.sem_num  = sem_number;  // the number of the semaphore to operate on
        sops.sem_op  = -1;      // operation
        sops.sem_flg  = SEM_UNDO;

        if(semop(sem_id, &sops, nsops) == -1)
        {
                fprintf(stderr,"sem_p failed\n");
                return 0;
        }
        return 1;
}

