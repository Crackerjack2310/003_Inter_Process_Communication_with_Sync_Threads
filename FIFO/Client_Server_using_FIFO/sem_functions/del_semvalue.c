#include"header.h"
#include"structures.h"

void del_semvalue(int sem_number)
{
	int sem_id;
	sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT);
        if(sem_id == -1) // setting semaphore value
	{
                fprintf(stderr,"Failed to get sem_id\n");	// initializing semaphore, putting semval = val
                exit(EXIT_FAILURE);
        }
	union semun sem_union;
	if(semctl(sem_id, sem_number, IPC_RMID, sem_union) == -1)
        {
                perror("del_semvalue()");
                fprintf(stderr,"Failed to delete semaphore\n");
        }
}
