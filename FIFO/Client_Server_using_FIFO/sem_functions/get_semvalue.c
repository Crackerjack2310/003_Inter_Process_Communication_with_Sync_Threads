#include"header.h"
#include"structures.h"

int get_semvalue(int sem_number)
{
	int sem_id, sem_value;
	sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT);
        if(sem_id == -1) // setting semaphore value
	{
                fprintf(stderr,"Failed to get sem_id\n : %d",errno);	// initializing semaphore, putting semval = val
                exit(EXIT_FAILURE);
        }
        
	if((sem_value = semctl(sem_id, sem_number, GETVAL)) == -1)
	{      
		perror("semctl(GETVAL)");
                fprintf(stderr,"Failed to get semval\n : %d",errno);	
		exit(EXIT_FAILURE);
       	}	
	return sem_value;
}
