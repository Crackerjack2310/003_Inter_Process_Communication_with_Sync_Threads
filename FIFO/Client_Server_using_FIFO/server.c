#include"header.h"
#include"structures.h"
#include"declarations.h"

int main() 		// server begins
{
	signal(SIGINT,sig_handler); 		// for deleting the semaphores
	printf("\n%s : Begin \n",__func__); 
	make_fifos();				// create all 
	int sem_id, clients = 1, count, fd1, fd2, result;
	pid_t ret;
	request r;
	receive res;
	system("clear");
	sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT);	
	
	if(sem_id == -1)	 // setting semaphore value
	{
		fprintf(stderr,"Failed to get sem_id\n");	// initializing semaphore, putting semval = val
        	exit(EXIT_FAILURE);
      	}
	printf("got sem_id : %d\n",sem_id);
	
	if(!set_semvalue(0))     // setting semaphore value for using it the first time.
	{
       		fprintf(stderr,"Failed to initialize semaphore at 0\n");// initializing semaphore, putting semval = val
        	exit(EXIT_FAILURE);
      	}

	if(!set_semvalue(1))     // setting semaphore value for using it the first time.
	{
       		fprintf(stderr,"Failed to initialize semaphore at 1\n");// initializing semaphore, putting semval = val
		exit(EXIT_FAILURE);
      	}

	while(1)		// infinite loop for server
	{
		res = req_processing();
	//	printf("Client PID : %d\n",res.pid);
	//	printf("result from server : %d\n",res.result);
	//	printf("server waiting for client : %d to open for receive \n",res.pid);
	
		printf("%s : PID : %d says : semvalue before p(1): %d\n", __FILE__, res.pid, get_semvalue(1));

		if(!sem_p(1)) 	/*get semaphore 1 ...here the process will wait for the value of semaphore
		to get to 1, if its already 1 the process will move into the critical section simultaneously
	       	decrementing the value by 1, thus the nest process will wait*/
			exit(EXIT_FAILURE);

       		printf("%s : PID : %d says : semvalue after p(1): %d\n", __FILE__, res.pid, get_semvalue(1));


// *******************************	CRITICAL SECTION 1 BEGINS	****************************************


      		printf("%s says :  PID : %d got semaphore 1\n",__FILE__,res.pid);

		fd2 = open("client_receive",O_WRONLY);		// Block on OPEN
		printf("client opened for receiving \n");
		count = write(fd2,&res,sizeof(res)); 		// writing the receive structure with server PID
		close(fd2);

		printf("\nServer wrote %d bytes to the requesting client no : %d\n\v",count,clients);
		printf("\v############################################################\v\n");
		clients++;

	}
	printf("\n%s : End \n",__func__);
	return 0;
}

void sig_handler(int sig)  // as we need SIGINT to terminate server as of now. hence it deletes the semaphore as well.
{
	del_semvalue(0);
	printf("\nSemaphore at 0 deleted Successfully\n");
	del_semvalue(1);
	printf("Semaphore at 1 deleted Successfully\n");
	exit(EXIT_SUCCESS);

}
