#include"header.h"
#include"structures.h"
#include"declarations.h"

void make_fifos() 
{
	printf("\n%s : Begin \n",__func__); 

	int ret;
	
	if(access("client_send",F_OK) == -1)	
        {
                ret = mkfifo("client_send",0666);    // rw permissions for all probable users

		if(ret == -1)

		{
       	                perror("client_send");
               	        exit(EXIT_FAILURE);
      		}
	 }
	

	if(access("client_receive",F_OK) == -1)
        {
                ret = mkfifo("client_receive",0666);    // rw permissions for all probable users

		if(ret == -1)
       	        {
       	                perror("client_receive");
       	                exit(EXIT_FAILURE);
       	        }
	}
		
	
	if(access("proc_read",F_OK) == -1)
        {
                ret = mkfifo("proc_read",0666);    // rw permissions for all probable users

       	        if(ret == -1)
       	        {
       	                perror("proc_read");
       	                exit(EXIT_FAILURE);
       	        }
	}
		
	if(access("proc_write",F_OK) == -1)
        {
                ret = mkfifo("proc_write",0666);    // rw permissions for all probable users

       	        if(ret == -1)
	        {
       	 	       perror("proc_write");
        	       exit(EXIT_FAILURE);
       		}
	}

	printf("\n%s : End \n",__func__); 
}	
