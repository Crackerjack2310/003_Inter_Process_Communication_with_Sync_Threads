#include"header.h"
#include"semun.h"

static int set_semvalue(void);
static void del_semvalue(void);
static int semaphore_p(void);
static int semaphore_v(void);
static int sem_id;

int main(int argc, char *argv[])
{
	int i;
	int pause_time;
	char op_char = 'O';
	srand((unsigned int)getpid()); // PID for process is set as a seed value for rand()
	sem_id = semget((key_t)1234, 1, 0666 | IPC_CREAT); // create semaphore, and provide a stream to the process to work on the semaphore, i.e. sem_id

	
	if(!set_semvalue())	// setting semaphore value for using it the first time.
	{
		fprintf(stderr,"Failed to initialize semaphore");// initializing semaphore, putting semval = val
		exit(EXIT_FAILURE);
	}

	if(argc > 1) 		// true for the process that takes atleast an argument
	{
		op_char = 'X'; /// op_char value is 'X' for process that takes 1 argument or more
		sleep(2);	
	}


//////////////////////////////////// 2 ///////////////////////////////////////////////
	

	for(i = 0; i < 10; i++)
	{


		if(!semaphore_p())  // here the process will get stuck if the semaphore value, i.e. value for semaphore is -1 (aready decremented by someother process) otherwise the process will get the semaphore value and proceed, setting the value as -1.
			exit(EXIT_FAILURE);

//**************************************************************************---- CRITICAL REGION BEGINS		

	
		printf("%c",op_char);
		fflush(stdout);
		pause_time = rand() % 3;
		sleep(pause_time);
		printf("%c",op_char); 
		fflush(stdout);
		printf("--");
		fflush(stdout);


//******************************************************************************---- CRITICAL REGION END	


///////////////////////////////////////////////////// 3 /////////////////////////////////////////////

		if(!semaphore_v()) // semaphore released
			exit(EXIT_FAILURE);	// semaphore value has been restored to 1, npw available for waiting processes.
	
		pause_time = rand() % 2;
		sleep(pause_time);

	}

	printf("\n%d --- finished \n",getpid());
	
	if(argc > 1)
	{
		sleep(10);
		del_semvalue();
		printf("Semaphore Deleted Successfully\n");
	}
	
	exit(EXIT_SUCCESS);

	return 0;
}

static int set_semvalue(void)
{
        union semun sem_union;
        sem_union.val = 1;
        if(semctl(sem_id, 0, SETVAL, sem_union) == -1)
        {
                printf("sem_id : %d\n",sem_id);
                return 0;
        }
                return 1;
}



static void del_semvalue(void)
{
        union semun sem_union;
        sem_union.val = 1;
        if(semctl(sem_id, 0, IPC_RMID, sem_union) == -1)
        {
                fprintf(stderr,"Failed to delete semaphore");
                exit(EXIT_FAILURE);
        }
}


////////////////////////////////////////////////// 6 ///////////////////////////////

static int semaphore_p(void)
{
        struct sembuf sem_b;
        sem_b.sem_num  = 0;
        sem_b.sem_op  = -1;
        sem_b.sem_flg  = SEM_UNDO;

        if(semop(sem_id, &sem_b, 1) == -1)
        {
                fprintf(stderr,"semaphore_p failed\n");
                return 0;
        }



static void del_semvalue(void)
{
        union semun sem_union;
        sem_union.val = 1;
        if(semctl(sem_id, 0, IPC_RMID, sem_union) == -1)
        {
                fprintf(stderr,"Failed to delete semaphore");
                exit(EXIT_FAILURE);
        }
}


////////////////////////////////////////////////// 6 ///////////////////////////////

static int semaphore_p(void)
{
        struct sembuf sem_b;
        sem_b.sem_num  = 0;
        sem_b.sem_op  = -1;
        sem_b.sem_flg  = SEM_UNDO;

        if(semop(sem_id, &sem_b, 1) == -1)
        {
                fprintf(stderr,"semaphore_p failed\n");
                return 0;
        }



static int semaphore_v(void)
{
        struct sembuf sem_b;
        sem_b.sem_num  = 0;
        sem_b.sem_op  = 1;
        sem_b.sem_flg  = SEM_UNDO;

        if(semop(sem_id, &sem_b, 1) == -1)
        {
                fprintf(stderr,"semaphore_v failed\n");
                return 0;
        }
        return 1;
}


                                                                                                        71,1          Bot

