#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

#define SIZE 20*1024*1024

int main()
{
	int ret,fd, count;
	char buff[1];
	if(access("myfifo",F_OK) == -1)// means file not exists then create, return 0 in case true
	{
		ret = mkfifo("myfifo", 0666);
		if(ret == -1)
		{
			perror("Fifo Failed");
			exit(EXIT_FAILURE);
		}
		printf("Fifo created successfully \n");
	}
	else
		printf("Fifo Already present \n");
		
	fd = open("myfifo",O_WRONLY);
	if(fd == -1)
	{
			perror("Open() failed");
			exit(EXIT_FAILURE);		
	}	
        printf("%s :myfifo opened success with fd: %d\n",__FILE__,fd);

        while(count < SIZE)
        {        
                ret = write(fd,buff,1);
                if(ret == -1)
                {
                                perror("write");
                                exit(EXIT_FAILURE);
                }
                count += ret;
                printf("%s : Write Bytes: %s : Count : %d\n",__FILE__,buff,count);
        }

	close(fd);
	printf("End : %s\n",__FILE__);
	return 0;
}
