#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#define SIZE 1024*1024*20

void main()
{

	int l = 0,ret,wfd,count;
	char *buff = " ";	
	if(access("myfifo",F_OK) == -1)
	{
		ret = mkfifo("myfifo",0666);	// rw permissions for all probable users
	
		if(ret == -1)
		{
			perror("mkfifo");
			exit(EXIT_FAILURE);	
		}
		printf("fifo created successfully !!\n");	
	}
	
	printf("fifo already present \n");
	
	wfd = open("myfifo",O_WRONLY);

	if(wfd == -1)
	{
		perror("open()");
		exit(EXIT_FAILURE);	
	}
	
	while(l < SIZE)
	{	
		count = write(wfd,buff,1);
		l = l + count;
		printf("\nbytes written = %d",l);
	}

	close(wfd);
//	printf("%s : func wrote %d bytes as : %s\n",__func__,count,buff);	

}
