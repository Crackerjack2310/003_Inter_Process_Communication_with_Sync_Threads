#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

void main()
{

	int ret,wfd,rfd,count;
	char buff[50];	
	memset(buff,'\0',50);

	if(access("FIFO1",F_OK) == -1)
	{
		ret = mkfifo("FIFO1",0666);	// rw permissions for all probable users
	
		if(ret == -1)
		{
			perror("FIFO1");
			exit(EXIT_FAILURE);	
		}
		printf("FIFO1 created successfully !!\n");
		
	}
	else
                printf("FIFO1 already present \n");

	
	if(access("FIFO2",F_OK) == -1)
	{
		ret = mkfifo("FIFO2",0666);	// rw permissions for all probable users
	
		if(ret == -1)
		{
			perror("FIFO2");
			exit(EXIT_FAILURE);	
		}
		printf("FIFO2 created successfully !!\n");
		
	}
	else
                printf("FIFO2 already present \n");

	
	sleep(2);	
	
	system("clear");
	printf("\n Wanna chat ??\n To talk : write text and press enter to send\n To listen : just press enter \n \n\v");
	
	fgets(buff,50,stdin);
	
	if(*buff == 10)	// we read first
	{
		system("clear");
		printf(" Cool..!! U decided to listen first \n Pls wait for your messages...\n You will get your prompt later.!!\n\n\v ");
		rfd = open("FIFO1",O_RDONLY);	// read from FIFO1
		if(rfd == -1)
		{
				perror("open()");
				exit(EXIT_FAILURE);	
		}

		wfd = open("FIFO2",O_WRONLY);// write to FIFO2
		if(wfd == -1)
		{
				perror("open()");
				exit(EXIT_FAILURE);	
		}
		while(1)	// reader in action
	        {

/////////////////////////////////////////////////////////////////// --- reading section    
             
			count = read(rfd,buff,50);
       		        if(strncmp("end",buff,3) == 0)
                	        break;
          	   	*(buff + count) ='\0';
             	  	printf("Friend  -->>  %s\n",buff);

//////////////////////////////////////////////////////////////////// --- writing section

            	    	printf("\nMe  -->>  ");
			fgets(buff,50,stdin);
			write(wfd,buff,(int)strlen(buff));
       			if(strncmp("end",buff,3) == 0)
                       		break;
               		sleep(1);

        	}	
        	close(rfd);
        	close(wfd);
	}		

	
	else		// we write first
	{
		system("clear");
		rfd = open("FIFO2",O_RDONLY);	// read from FIFO2
		if(rfd == -1)
		{
				perror("open()");
				exit(EXIT_FAILURE);	
		}

		wfd = open("FIFO1",O_WRONLY);// write to FIFO1
		if(wfd == -1)
		{
				perror("open()");
				exit(EXIT_FAILURE);	
		}
		system("clear");
		printf(" Cool..!! U decided to initiate the talk \nHere's your prompt ...!!\n\n\v ");
		
		while(strncmp("end",buff,3))	// Writer in action
		{
		
/////////////////////////////////////////////////////////////////// --- sending section

			printf("\nMe  -->>  ");
			fgets(buff,50,stdin);
			write(wfd,buff,(int)strlen(buff));
               		if(strncmp("end",buff,3) == 0)
                		break;

/////////////////////////////////////////////////////////////////// --- reveiving section

			sleep(1);
			count = read(rfd,buff,50);
               	 	if(strncmp("end",buff,3) == 0)
                        	break;
                	*(buff + count) ='\0';
             		printf("Friend  -->>  %s\n",buff);		
		}	
	
		close(rfd);
		close(wfd);
	}

}
