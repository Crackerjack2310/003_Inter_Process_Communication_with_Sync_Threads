#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

void main()
{

	int ret,rfd,count;
	char *rbuff;
	rbuff = (char*)malloc(sizeof(char)*50);	
	
	if(access("myfifo",F_OK) == -1)
	{
		ret = mkfifo("myfifo",0666);	// rw permissions for all probable users
	
		if(ret == -1)
		{
			perror("mkfifo");
			exit(EXIT_FAILURE);	
		}
		printf("\nfifo created successfully !!\n");
		sleep(2);
	}
	else
		printf("fifo already present \n");

	sleep(2);
	system("clear");
	printf("Awaiting Sender !!\n");	
	rfd = open("myfifo",O_RDONLY);
	system("clear");
	printf("\nSender Active ...Listening now !!\n\v");	

	if(rfd == -1)
	{
		perror("open()");
		exit(EXIT_FAILURE);	
	}
	while(1)
	{
		count = read(rfd,rbuff,50);
		if(strncmp("end",rbuff,3) == 0)
		{
		printf("\n############## Conversation Ended by Sender ################\n");
		break;
		}
		*(rbuff + count) ='\0';
		printf("Receiver -->>  %s\n",rbuff);	
	}
	sleep(2);
	close(rfd);

}

