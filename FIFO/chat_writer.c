#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

void main()
{

	int ret,wfd,count;
	char buff[50];	
	
	if(access("myfifo",F_OK) == -1)
	{
		ret = mkfifo("myfifo",0666);	// rw permissions for all probable users
	
		if(ret == -1)
		{
			perror("mkfifo");
			exit(EXIT_FAILURE);	
		}
		printf("fifo created successfully !!\n");
	}
	else
		printf("fifo already present \n");
	
	sleep(2);
	system("clear");
	printf("Awaiting receiver !! \n");
	
	wfd = open("myfifo",O_WRONLY);
	system("clear");
	
	printf("\nReceiver active..Ready to send !! \n\v");
	
	if(wfd == -1)
	{
		perror("open()");
		exit(EXIT_FAILURE);	
	}
	
	while(strncmp("end",buff,3))
	{
		printf("Sender -->>  ");
		fgets(buff,50,stdin);
		write(wfd,buff,(int)strlen(buff));
		
	}
	printf("\n############## Conversation Ended by Sender ################\n");	
	sleep(2);
	close(wfd);

}
