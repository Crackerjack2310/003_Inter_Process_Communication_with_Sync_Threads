#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#define SIZE 1024*1024*20

void main()
{

	int ret,rfd,count,l = 0;
	char *rbuff;
	rbuff = (char*)malloc(sizeof(char)*1024);	
	
	if(access("myfifo",F_OK) == -1)
	{
		ret = mkfifo("myfifo",0666);	// rw permissions for all probable users
	
		if(ret == -1)
		{
			perror("mkfifo");
			exit(EXIT_FAILURE);	
		}
		printf("fifo created successfully !!\n");	
	}
	
	printf("fifo already present \n");
		
	rfd = open("myfifo",O_RDONLY);
	sleep(5);
	if(rfd == -1)
	{
		perror("open()");
		exit(EXIT_FAILURE);	
	}
	
	while(l < SIZE)
	{
		count = read(rfd,rbuff,1);
		l = l + count;
		printf("\nbytes read = %d\n",l);
	}

//	*(rbuff + count) ='\0';
//	printf("%s : func read %d bytes as : %s\n",__func__,count,rbuff);	
	
	close(rfd);
}
