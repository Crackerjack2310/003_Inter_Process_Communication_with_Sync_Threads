#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

void main()
{

	int ret,fd,count;
	char *buff;
	buff = (char*)malloc(sizeof(char)*50);	
	
	if(access("myfifo",F_OK) == -1)
	{
		ret = mkfifo("myfifo",0666);	// rw permissions for all probable users
	
		if(ret == -1)
		{
			perror("mkfifo");
			exit(EXIT_FAILURE);	
		}
		printf("FIFO created successfully !!\n");	
	}
	else
                printf("FIFO already present \n");

	sleep(2);
	system("clear");
	printf("Awaiting signal !! \n");
	fd = open("myfifo",O_RDWR);
	
	if(fd == -1)
	{
		perror("open()");
		exit(EXIT_FAILURE);	
	}
	system("clear");	
	
	printf("\nReady to talk !! \n\v");
	
	while(1)
	{
		
//////////////////////////////////////// --- reading section	
		count = read(fd,buff,50);
		if(strncmp("end",buff,3) == 0)
			break;
		*(buff + count) ='\0';
		printf("Receiver -->>  %s\n",buff);	
	
//////////////////////////////////////// --- writing section

		printf("\nsender -->>  ");
                fgets(buff,50,stdin);
                write(fd,buff,(int)strlen(buff));
		if(strncmp("end",buff,3) == 0)
			break;
		sleep(1);	

	}
	close(fd);
}
