#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

void main()
{

	int ret,fd,count;
	char buff[50];	
	memset(buff,'\0',50);

	if(access("myfifo",F_OK) == -1)
	{
		ret = mkfifo("myfifo",0666);	// rw permissions for all probable users
	
		if(ret == -1)
		{
			perror("mkfifo");
			exit(EXIT_FAILURE);	
		}
		printf("FIFO created successfully !!\n");
		
	}
	else
                printf("FIFO already present \n");
	
	sleep(2);	
	system("clear");
	printf("Awaiting signal !! \n");
	
	fd = open("myfifo",O_RDWR);
	system("clear");
	
	if(fd == -1)
	{
		perror("open()");
		exit(EXIT_FAILURE);	
	}
	printf("\n Wanna chat ??\n To talk : write text and press enter to send\n To listen : just press enter \n \n\v");
	
	fgets(buff,50,stdin);
	
	if(*buff == 10)	// we read first
	{
		system("clear");
		printf(" Cool..!! U decided to listen first \n Pls wait for your messages...\n You will get your prompt later.!!\n\n\v ");
//		sleep(2);
//		system("clear");
//		printf("\n Here's your prompt !!\n");
		while(1)	// reader in action
	        {

/////////////////////////////////////////////////////////////////// --- reading section    
             
			count = read(fd,buff,50);
       		        if(strncmp("end",buff,3) == 0)
                	        break;
          	   	*(buff + count) ='\0';
             	  	printf("Friend  -->>  %s\n",buff);

//////////////////////////////////////////////////////////////////// --- writing section

            	    	printf("\nMe  -->>  ");
			fgets(buff,50,stdin);
			write(fd,buff,(int)strlen(buff));
       			if(strncmp("end",buff,3) == 0)
                       		break;
               		sleep(1);

        	}	
        	close(fd);
	}		

	
	else		// we write first
	{
//		printf("\nsender -->> %s\n\n",buff);
//		write(fd,buff,(int)strlen(buff));
		
		system("clear");
		printf(" Cool..!! U decided to initiate the talk \nHere's your prompt ...!!\n\n\v ");
		while(strncmp("end",buff,3))	// Writer in action
		{
		
/////////////////////////////////////////////////////////////////// --- sending section

			printf("\nMe  -->>  ");
			fgets(buff,50,stdin);
			write(fd,buff,(int)strlen(buff));
               		if(strncmp("end",buff,3) == 0)
                		break;

/////////////////////////////////////////////////////////////////// --- reveiving section

			sleep(1);
			count = read(fd,buff,50);
               	 	if(strncmp("end",buff,3) == 0)
                        	break;
                	*(buff + count) ='\0';
             		printf("Friend  -->>  %s\n",buff);		
		}	
	
		close(fd);
	}

}
