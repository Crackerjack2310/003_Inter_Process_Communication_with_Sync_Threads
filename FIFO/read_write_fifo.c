#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>

void main()
{

	int ret,fd,count;
	char *rbuff,*buff = "This is a test for writing into FIFO";	
	if(access("myfifo",F_OK) == -1)
	{
		ret = mkfifo("myfifo",0666);	// rw permissions for all probable users
	
		if(ret == -1)
		{
			perror("mkfifo");
			exit(EXIT_FAILURE);	
		}
		printf("fifo created successfully !!\n");	
	}
	rbuff = (char*)malloc(sizeof(char)*1024);
	printf("fifo already present \n");
	
	fd = open("myfifo",O_RDWR);

	if(fd == -1)
	{
		perror("open()");
		exit(EXIT_FAILURE);	
	}
	
	count = write(fd,buff,(int)strlen(buff));
	if(count == -1)
	{
		perror("write()");
		exit(EXIT_FAILURE);
	}	
	printf("%s : func wrote %d bytes as : %s\n",__func__,count,buff);	
	
        count = read(fd,rbuff,1024);
        *(rbuff + count) ='\0';
        printf("%s : func read %d bytes as : %s\n",__func__,count,rbuff);

        close(fd);

}
