#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>

void main()
{

	printf("\n%s : Begin    PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
	
	pid_t ret;
	
	printf("\nHello !! \n");
	
	ret=fork();

	switch(ret)
	{
	
	case -1:
		perror("fork failed");
		exit(EXIT_FAILURE);

	case  0:
		printf("\n Child \n");
		break;
	default:
		printf("\n Parent \n");
		break;
	}

	printf("\nHello World..!!\n");

	printf("\n%s : End     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
}


