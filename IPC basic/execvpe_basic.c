#define _GNU_SOURCE
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>

void main()
{
	//extern char **environ;	
	printf("\n%s : Begin     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());

	char *args[2];
	char *path;
//	path = getenv("PATH");
//	printf("\npath %s\n",path);
	char *const envp[]={NULL};

	args[0]="e1";
	args[1]=NULL;

	printf("\nBefore exec\n");

	execvpe("e1",args,envp);
	perror("execvpe");
	printf("\n Failed \n");
	
	printf("\n%s : End     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
}


