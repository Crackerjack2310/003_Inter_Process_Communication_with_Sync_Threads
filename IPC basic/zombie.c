#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>

void main()
{

	printf("\n%s : Begin    PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
	
	pid_t ret;

	int v;
	printf("\nHello !! \n");
	
	ret=fork();
	
	printf("\nReturned value: %d\n",ret);

	switch(ret)
	{
	
		case -1:
			perror("fork failed");
			exit(EXIT_FAILURE);

		case  0:
			printf("\n Child \n");
			v=5;	
			break;
		default:
			printf("\n Parent \n");
			v=15;
			
	}
	
	for(int i=0;i<v;i++)
	{
		printf("\n PID : %d     PPID : %d",getpid(),getppid());
		sleep(1);
	}


	printf("\nHello World..!!\n");

	printf("\n%s : End     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
}


