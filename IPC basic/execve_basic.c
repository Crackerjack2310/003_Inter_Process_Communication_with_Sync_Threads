#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>

void main()
{
	printf("\n%s : Begin     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());

	char *args[2];
	char *envp[]={"PATH=/bin","TERM=console",NULL};
	//char *envp[]={"NULL"};

//	system("echo $PATH");
//	system("pwd");
	args[0]="e1";
	args[1]=NULL;

	printf("\nBefore exec\n");

	execve("e1",args,envp);
	perror("execve");
	
	printf("\n%s : End     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
}


