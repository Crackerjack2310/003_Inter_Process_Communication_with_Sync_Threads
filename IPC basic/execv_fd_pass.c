#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<fcntl.h>
#include<string.h>

int main()
{
	
	printf("\n%s : Begin     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());

	int fd,count,len;
	char *sfd,*buff="Consistency is the key",*scount,*args[4];
	sfd=(char*)malloc(sizeof(int));
	
	scount=(char*)malloc(sizeof(int));
	
	fd=open("myfile",O_RDWR|O_CREAT);
	
	if(fd==-1)
	{
		perror("Open Failed");
		exit(EXIT_FAILURE);
	}

	sprintf(sfd,"%d",fd);
	len=strlen(buff);
	
	sprintf(scount,"%d",len);
	count = write(fd,buff,len);
	
	printf("\nbytes written : %d\n",count);

	args[0]="e4";
	args[1]=sfd;
	args[2]=scount;
	args[3]=NULL;

	execv("./e4",args);
	
	printf("\n%s : End     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
}


