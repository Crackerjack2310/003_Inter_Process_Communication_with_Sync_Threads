#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<fcntl.h>
#include<string.h>

void main(int argc,char *argv[])
{

	printf("\n%s : Begin pid:%d  ppid%d \n",__FILE__, getpid(), getppid());
		
	int fd,len,count;
	char *buff;
	
	buff=(char*)malloc(sizeof(char)*100);
	
	fd = atoi(argv[1]);
	len = atoi(argv[2]);
	
	lseek(fd,0,SEEK_SET);
	count = read(fd,buff,len);	

	printf("\nNo of bytes read: %d\n",count);
	
	printf("\nRead Text : %s\n",buff);
	
	printf("\n%s : End \n",__FILE__);
}
