#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>

void main()
{

	printf("\n%s : Begin    PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
	
	pid_t ret,ret1,ret2;
	
	ret = fork();
	
	switch(ret)
	{
		case -1:
			perror("fork failed");
			exit(EXIT_FAILURE);
		case  0:
			printf("\nChild1 with PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
			break;
		default:
			printf("\nmain() with PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
		
	}


	ret = fork();

	switch(ret)
	{
		case -1:
			perror("fork failed");
			exit(EXIT_FAILURE);
		case  0:
			printf("\nChild2 with PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
			break;
		default:
			printf("\nParent with PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
	}
	
	printf("\n%s : End     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
}


