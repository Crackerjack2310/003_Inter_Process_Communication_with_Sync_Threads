#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>

void main()
{
	
	printf("\n%s : Begin     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());

	char *args[2];
	args[0]="e1";
	args[1]=NULL;

	printf("\nBefore exec\n");

	execvp("e1",args);
	printf("\n After exec\n");
	
	printf("\n%s : End     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
}


