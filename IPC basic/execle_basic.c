#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>

void main()
{
	printf("\n%s : Begin     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());

	
	char *envp[]={"PATH=/home/xenial-xerus/Desktop","TERM=console",NULL};

	printf("\nBefore exec\n");

	execle("e1","e1",NULL,envp);
	printf("\n Failed \n");

	perror("execle");	

	printf("\n%s : End     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
}


