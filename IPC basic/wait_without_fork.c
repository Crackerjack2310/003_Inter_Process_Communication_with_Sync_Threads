#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

void main()
{

	printf("\n%s : Begin    PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
	
	pid_t ret=5,wt=3;
	
	int ptr=7;
	printf("\nwt value : %d\n",wt);

	int v;
	//printf("\n Hello !! \n");
	
	//ret=fork();
	
	//printf("\nReturned value: %d\n",ret);

	switch(ret)
	{
	
		case -1:
			perror("fork failed");
			exit(EXIT_FAILURE);

		case  0:
		//	printf("\n Child \n");
			v=15;	
			break;
		default:
		//	printf("\n Parent \n");
			v=5;
			
	}
	
	for(int i=0;i<v;i++)
	{
	printf("\n PID : %d     PPID : %d\n",getpid(),getppid());
		sleep(1);
	}
	wt=wait(&ptr);
	
	printf("\nptr value : %d\n",ptr);
	printf("\nwt value : %d\n",wt);
//	printf("\nHello World..!!\n");

	printf("\n%s : End     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
}


