#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

void main()
{

	printf("\n%s : Begin    PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
	
	pid_t ret=5,child_pid=3;
	
	int exit_code=7;

	int v;
	
	ret=fork();
	
	switch(ret)
	{
	
		case -1:
			perror("fork failed");
			exit(EXIT_FAILURE);

		case  0:
			v=15;	
			break;
		default:
			v=5;
			
	}
	
	for(int i=0;i<v;i++)
	{
	printf("\n PID : %d     PPID : %d\n",getpid(),getppid());
		sleep(1);
	}
	
	child_pid=wait(&exit_code);

	
	if(ret)		//Parent only
	{
		if(WIFEXITED(exit_code))
			printf("\nChild process ended with exit code : %d\n",WEXITSTATUS(exit_code));
		
		else
			printf("\nChild process ended  abnormally\n");
	}
	
		
	
	printf("\nexit_code : %d\n",exit_code);
	printf("\nchild_pid value : %d\n",child_pid);

	printf("\n%s : End     PID: %d  PPID: %d  \n",__FILE__,getpid(),getppid());
}


