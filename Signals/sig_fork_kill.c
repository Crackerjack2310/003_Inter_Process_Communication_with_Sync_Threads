#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<signal.h>
#include<stdlib.h>

void sig_handler(int);

void main()
{
	signal(SIGINT,sig_handler);	
	pid_t ret,a;
	ret = fork();
	
	switch(ret)
	{
		case -1: 
			perror("Fork()");
			exit(EXIT_FAILURE);
		case 0:
				printf("This is child with PID : %d  PPID : %d \n",getpid(),getppid());
				sleep(5);
				kill(getppid(),SIGINT);
				break;
			
		default:
				printf("This is parent  waiting to be hit PID : %d PPID : %d \n",getpid(),getppid());
				pause();		
				printf("Parent was hit \n");
	}
}		

void sig_handler(int sig)
{
	printf("This is signal no : %d for PID : %d\n",sig,getpid());
}


