#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<signal.h>
#include<stdlib.h>

void sig_handler(int);

void main()
{
	struct sigaction act;
	act.sa_handler = sig_handler;	// choosing the sighandler, via function pointers
	sigaddset(&act.sa_mask, SIGQUIT);		// nothing to block
	act.sa_flags = 0;		// no flags set
		
	sigaction(SIGINT, &act, 0);		// signal registration
	
	while(1)
	{
		printf("I am god..unless signal initiated\n");
		sleep(1);
	}
}

void sig_handler(int sig)
{
	printf("This is signal handler: signal no : %d\n",sig);
	printf("Going to sleep for 10 secs...handler on\n");
	sleep(10);
}


