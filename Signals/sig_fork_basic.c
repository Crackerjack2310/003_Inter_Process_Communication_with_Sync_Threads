#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<signal.h>
#include<stdlib.h>

void sig_handler(int);

void main()
{
	signal(SIGINT,sig_handler);	
	signal(SIGTSTP,sig_handler);	
	pid_t ret;	
	ret = fork();
	
	switch(ret)
	{
		case -1: 
			perror("Fork()");
			exit(EXIT_FAILURE);
		case 0:
			while(1)
			{
				printf("\nThis is child with PID : %d PPID : %d\n",getpid(),getppid());
				sleep(1);
			}
			break;
		default:
			while(1)
			{
				printf("\nThis is parent with PID : %d PPID : %d\n",getpid(),getppid());
				sleep(1);
			}
	}		
}

void sig_handler(int sig)
{
	signal(SIGTSTP,SIG_DFL);
	printf("\nThis is signal handler: signal no : %d  PID : %d  PPID : %d \n",sig,getpid(),getppid());
}

