#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<signal.h>
#include<stdlib.h>

void sig_handler(int);

void main()
{
	signal(SIGINT,sig_handler);		// signal declarations	
	
	while(1)
	{
		printf("I am god..unless signal initiated\n");
		sleep(1);
	}
	
}

void sig_handler(int sig)
{
	printf("This is signal handler: signal no : %d\n",sig);
}


