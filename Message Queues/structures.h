///////////////////////////////////////////////////////////	Message queue structures


typedef struct msgbuf 
{
               long mtype;       /* message type, must be > 0 */
               char mtext[BUFSIZ];    /* message data */
}msgbuf;


///////////////////////////////////////////////////////////	Client - Server communication structures


typedef struct request
{
	char oper;
	int opr1;
	int opr2;
	pid_t pid;
}request;


typedef struct receive
{
	int result;
	pid_t pid;
}receive;


////////////////////////////////////////////////////////////   Semaphore structures


union semun {
               int              val;    /* Value for SETVAL */
               struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
               unsigned short  *array;  /* Array for GETALL, SETALL */
               struct seminfo  *__buf;  /* Buffer for IPC_INFO
                                           (Linux-specific) */
            };
