receive req_processing();

void make_fifos();

int set_semvalue(int);
void del_semvalue(int);
int sem_p(unsigned short);
int sem_v(unsigned short);

void sig_handler(int); // need a handler to terminate the server, thus need to delete the semaphores

