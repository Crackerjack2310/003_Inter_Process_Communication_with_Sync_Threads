#include"header.h"
#include"structures.h"
#include"declarations.h"
#include"sem_functions.h"
#define BUFSIZ 50
void main()
{
	msgbuf message;			// msg que of type msgbuf --->> 1.mtype  2.mtext
	int msqid, i = 1;
	int running = 1;
	msqid = msgget( (key_t) 1234, 0666 | IPC_CREAT ); /* create a msg que with user key 1234;
							and return kernel key in msqid*/
	if (msqid  == -1)
	{
		fprintf(stderr,"msgget() failed with error %d \n",errno);
		exit(EXIT_FAILURE);
	}	
	char buffer[BUFSIZ];
	memset(buffer,'\0',BUFSIZ);
	printf("BUFSIZ = %d\n\v", BUFSIZ);
	while(running)
	{
		printf("Sender -->>  ");
		fgets(buffer, BUFSIZ, stdin);
		strcpy(message.mtext, buffer);
		message.mtype = i++;
		if (msgsnd(msqid, &message, BUFSIZ, 0 ) == -1)
		{
			fprintf(stderr,"msgsnd failed with error %d \n",errno);
                	exit(EXIT_FAILURE);
		}
		if (strncmp(message.mtext,"end",3) == 0) 	// when writer enters "end"
			running = 0;
	}

	printf("###################### Conversation ended by writer ######################\n");
	
	exit(EXIT_SUCCESS);
}

