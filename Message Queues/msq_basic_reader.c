#include"header.h"
#include"structures.h"
#include"declarations.h"
#include"sem_functions.h"
#define BUFSIZ 50

void main()
{
	msgbuf message;			// msg que of type msgbuf --->> 1.mtype  2.mtext
	int msqid;
	int running = 1;
	long msg_type = 0 ;
	msqid = msgget( (key_t) 1234, 0666 | IPC_CREAT ); /* create a msg que with user key 1234;
							and return kernel key in msqid*/
	if (msqid  == -1)
	{
		fprintf(stderr,"Message failed with error %d \n",errno);
		exit(EXIT_FAILURE);
	}	
	
	while(running)
	{
		if (msgrcv(msqid, &message, BUFSIZ, msg_type, 0 ) == -1)
		{
			fprintf(stderr,"msgrcv failed with error %d \n",errno);
                	exit(EXIT_FAILURE);
		}
		printf("Receiver -->> %s", message.mtext);

		if (strncmp(message.mtext,"end",3) == 0) 	// when writer enters "end"
			running = 0;
	}

	if (msgctl(msqid, IPC_RMID, 0) == -1)		// remove the msg que ID
	{
		fprintf(stderr,"msgctl(IPC_RMID) failed with error %d \n",errno);
              	exit(EXIT_FAILURE);
	}
	else
		printf("Message queue deleted successfully\n");
	
	exit(EXIT_SUCCESS);
}

