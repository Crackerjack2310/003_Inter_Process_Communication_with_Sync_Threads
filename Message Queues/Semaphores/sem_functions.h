static int sem_id;

int set_semvalue(void)
{
        sem_id = semget((key_t) 1234, 1, IPC_CREAT | 0666 );
	union semun sem_union;
        sem_union.val = 1;
        if(semctl(sem_id, 0, SETVAL, sem_union) == -1)
	{
         	printf("sem_id : %d\n",sem_id);
	 	return 0;
	}
		return 1;
}


//////////////////////////////////////////////////////// 5 ////////////////////////


void del_semvalue(void)
{
        sem_id = semget((key_t) 1234, 1, IPC_CREAT | 0666 );
        union semun sem_union;
        sem_union.val = 1;
        if(semctl(sem_id, 0, IPC_RMID, sem_union) == -1)
	{
        	fprintf(stderr,"Failed to delete semaphore");
		exit(EXIT_FAILURE);
	}
}


////////////////////////////////////////////////// 6 ///////////////////////////////


int sem_p(void)
{
        sem_id = semget((key_t) 1234, 1, IPC_CREAT | 0666 );
        struct sembuf sem_b;
        sem_b.sem_num  = 0;
        sem_b.sem_op  = -1;
        sem_b.sem_flg  = SEM_UNDO;

        if(semop(sem_id, &sem_b, 1) == -1)
        {
                fprintf(stderr,"sem_p failed\n");
                return 0;
        }
        return 1;
}

int sem_v(void)
{
        sem_id = semget((key_t) 1234, 1, IPC_CREAT | 0666 );
        struct sembuf sem_b;
        sem_b.sem_num  = 0;
        sem_b.sem_op  = 1;
        sem_b.sem_flg  = SEM_UNDO;

        if(semop(sem_id, &sem_b, 1) == -1)
        {
                fprintf(stderr,"sem_v failed\n");
                return 0;
        }
        return 1;
}

 
