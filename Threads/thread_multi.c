#include"header.h"
#include"structures.h"
#include"declarations.h"

int i = 0;
int main()
{
	int jret, cret;
	pthread_t tid[10];
	int *retval;
	for (i = 0; i <= 9; i++)
	{
		cret = pthread_create(&tid[i], NULL, &start_func, (void*)&i);
		if (cret!= 0)
		{
			perror("pthread_create()");
			exit(EXIT_FAILURE);
		}
		sleep(1);
	}
	char message[10];
	for (i = 0; i <= 9; i++)
	{
		jret = pthread_join(tid[i],(void**)&retval);
		if (jret!= 0)
		{	
			perror("join()");
			exit(EXIT_FAILURE);
		}
		sprintf(message,"%d",*retval);
		printf("thread %d exited with exit msg : %s \n", i, message);
	}

	return 0;
}

void *start_func(void* a)
{
	int *arg;
	int i = 2;
	arg = (int*)&i;

	printf("this is thread no : %d\n",*arg);
	
	if (*arg == 0)
		sleep(12);		// first
	else
		sleep(1);
	pthread_exit((void*)&i);		// exiting the thread with msg : 
}
