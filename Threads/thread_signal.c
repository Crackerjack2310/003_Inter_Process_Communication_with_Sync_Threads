#include"header.h"
#include"structures.h"
#include"declarations.h"

int i = 0, join = 0;
pthread_t tid[20];
int main()
{
	int jret, jcret, cret;
	int thread_no;
	
	for (i = 0; i <= 9; i++)		//	for creating threads for each for start func
	{
		cret = pthread_create(&tid[i], NULL, start_func, (void*)&i);
		if (cret!= 0)
		{
			perror("pthread_create()");
			exit(EXIT_FAILURE);
		}
		sleep(0.5);
	}
	
	for (i = 0; i <= 9; i++) 	//	for pthread_join same as the number of threads created
	{
		jcret = pthread_create(&tid[i + 10], NULL, join_func, (void*)&i);
		if (jcret!= 0)
		{
			perror("pthread_create() join");
			exit(EXIT_FAILURE);
		}
		sleep(0.5);
	}
	
	while (1)
	{
		printf("enter thread no. to kill\n");
		scanf("%d",&thread_no);
		if (thread_no < 0 || thread_no > 9)
		{
			printf("Wrong thread choice...Exiting code\n");
			exit(EXIT_FAILURE);
		}
		pthread_kill(tid[thread_no],SIGINT);
	}
	return 0;
}

void *start_func(void* arg)
{
	signal(SIGINT,sig_handler);
	int thread_no = *(int*)arg;
	while (1)
	{
		printf("Executing thread number : %d\n",thread_no);
		sleep(2);
	}		
}

void *join_func(void* arg)		
{
	int i = *(int*)arg;
	printf("join handler started for : %d\n",i);
	int jret = pthread_join(tid[i], NULL);
	if (jret!= 0)
        {
        	perror("join()");
                exit(EXIT_FAILURE);
        }
        printf("Thread : %d was killed\n", i);
}

void sig_handler(int sig)
{	
	pthread_exit(NULL);		// exiting the thread with msg 
}
