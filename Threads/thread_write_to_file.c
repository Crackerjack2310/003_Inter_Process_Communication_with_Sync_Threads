#include"header.h"
#include"structures.h"
#include"declarations.h"

int main()
{
	int jret, jcret, cret;
	pthread_t tid;
	char buffer[50];

	system("rm -rf myfile");	
	while (1)
	{
		////////////////////////////////////////  mutex lock
		printf("enter text for the file\n");
		fgets(buffer, 50, stdin);
		printf("String length = %d\n", strlen(buffer));
		jret = pthread_create(&tid, NULL, start_func, (void*)buffer);
		if (jret!= 0)
		{
			perror("pthread_create()");
			exit(EXIT_FAILURE);
		}
		jret = pthread_join(tid, NULL);	// threads 
		if (jret!= 0)
       		{
       		 	perror("join()");
        	        exit(EXIT_FAILURE);
        	}
	//	printf("thread done\n");
	}
	return 0;
}

void *start_func(void* arg)
{
	char *buffer = arg;
	printf("String length 2 = %d\n", strlen(buffer));
	int fd, count; 
	
	if (strncmp("exit",buffer,4) == 0)
	{
		printf("Exited by user..file closed!!\n");
		close(fd);	
		exit(EXIT_FAILURE);
	}
	fd = open("myfile",O_WRONLY | O_CREAT | O_APPEND);
	if (fd == -1)
	{
		perror("open()");
		exit(EXIT_FAILURE);
	}
	count = write(fd,buffer,strlen(buffer));
	if (count == -1)
	{
		perror("write()");
		exit(EXIT_FAILURE);
	}
	close(fd);
	printf("wrote %d bytes to the file\n",count);
	pthread_exit(NULL);		// exiting the thread with msg 
}
