#include"header.h"
#include"structures.h"
#include"declarations.h"

int main() //	subtractor
{
	printf("\n%s : Begin \n",__FILE__); 
	
	int result, count, fd3, fd4;
	request *req;
	receive res;
	int shmid = shmget((key_t) 1234, BUFSIZ, 0666 | IPC_CREAT);
        if (shmid == -1)
        {
        	perror("shmget()");
                exit(EXIT_FAILURE);
        }
	req = (request*)shmat(shmid, NULL, 0);	// for input

	req = req + (sizeof(request) + sizeof(receive) + sizeof(request));
	
	while(req->c_flag);
	
	req->opr1 = (req->opr1 - req->opr2);
	printf("\n%s : End \n",__FILE__);
		
	return 0;
}
