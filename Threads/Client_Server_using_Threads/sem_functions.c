#include"header.h"
#include"structures.h"
#include"declarations.h"

static int sem_id;
size_t nsops = 1; // only one semaphore operation to be performed

int sem_p(unsigned short sem_number)
{
	sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT);
       	if(sem_id == -1) 
	{
		fprintf(stderr,"Failed to get sem_id\n"); // initializing semaphore, putting semval = val
                exit(EXIT_FAILURE);
        }
	struct sembuf sops;	// pointer to the array that has each element as struct sembuf
	sops.sem_num  = sem_number;  // the number of the semaphore to operate on
        sops.sem_op  = -1;	// operation
        sops.sem_flg  = SEM_UNDO;	
	
        if(semop(sem_id, &sops, nsops) == -1)
        {
                fprintf(stderr,"sem_p failed\n");
                return 0;
        }
        return 1;

       	printf("%s : Ended\n",__func__);
}

int sem_v(unsigned short sem_number)
{
	sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT);
        if(sem_id == -1) // setting semaphore value
	{					// setting semaphore value for using it the first time
                fprintf(stderr,"Failed to get sem_id\n");// initializing semaphore, putting semval = val
                exit(EXIT_FAILURE);
        }
        
	struct sembuf sops;	// pointer to the array that has each element as struct sembuf
	sops.sem_num  = sem_number;  // the number of the semaphore to operate on
        sops.sem_op  = 1;	// operation
        sops.sem_flg  = SEM_UNDO;

        if(semop(sem_id, &sops, nsops) == -1)
        {
                fprintf(stderr,"sem_v failed\n");
                return 0;
        }
        return 1;
       	printf("%s : Ended\n",__func__);
}

int get_semvalue(unsigned short sem_number)
{
        int sem_id, sem_value;
        sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT);
        if(sem_id == -1) // setting semaphore value
        {
                fprintf(stderr,"Failed to get sem_id\n : %d",errno);    // initializing semaphore, putting semval = val
                exit(EXIT_FAILURE);
        }

        if((sem_value = semctl(sem_id, sem_number, GETVAL)) == -1)
        {
                perror("semctl(GETVAL)");
                fprintf(stderr,"Failed to get semval\n : %d",errno);
                return 0;
        }
        return sem_value;
}

int set_semvalue(int sem_number)
{
        sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT);
        if(sem_id == -1) // setting semaphore value
        {
                fprintf(stderr,"Failed to get sem_id\n");       // initializing semaphore, putting semval = val
                exit(EXIT_FAILURE);
        }
        union semun sem_union;
        sem_union.val = 1;
        if(semctl(sem_id, sem_number, SETVAL, sem_union) == -1)
        {
                printf("sem_id : %d",sem_id);
                perror("semctl()");
                return 0;

        }       return 1;
}

void del_semvalue(int sem_number)
{
        sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT);
        if(sem_id == -1) // setting semaphore value
        {
                fprintf(stderr,"Failed to get sem_id\n");       // initializing semaphore, putting semval = val
                exit(EXIT_FAILURE);
        }
        union semun sem_union;
        if(semctl(sem_id, sem_number, IPC_RMID, sem_union) == -1)
        {
                perror("del_semvalue()");
                fprintf(stderr,"Failed to delete semaphore\n");
        }
}

