#include"header.h"
#include"structures.h"
#include"declarations.h"

int main() 		// client1 
{
	printf("\n%s : PID : %d Begin \n", __FILE__, getpid()); 

	receive *res;
	request *req;

	int shmid, result;
	shmid = shmget((key_t) 1234, BUFSIZ, 0666 | IPC_CREAT);
	req = (request*)shmat(shmid, NULL, 0);

/////////////////////////////////////////////////////////////////	incoming intimation by setting drone_flag
									// add mutex_lock
	int sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT);

        if(sem_id == -1) // setting semaphore value for using it the first time.
        {
                fprintf(stderr,"Failed to get sem_id\n");// initializing semaphore, putting semval = val
                exit(EXIT_FAILURE);
        }


        if(!sem_p(0))   /*Semaphore 1 released as value restored to sem_value, now available
                        for the server willing to write to the FIFO client_receive*/
                exit(EXIT_FAILURE);
	
	printf("\n%s : PID : %d got semaphore\n", __FILE__, getpid());

	while(!req->drone_flag);
	req->drone_flag = 0;	

///////////////////////////////////////////////////////////////// write result

	req->oper = '/';	// operation to be performed
	req->opr1 = 10 ;		// operand 1
	req->opr2 = 5 ;		// operand 2
	req->pid = getpid();		// client's PID
	req->c_flag = 0; 		// putting this as zero enables the req_processing to come out of infnite
					// while loop, this is the confirmation that the client has written the data
					// onto the shared memory

//////////////////////////////////////////////////////////////////// read result

	printf("%s : PID : %d says operation to be performed : %c\n", __FILE__, getpid(), req->oper);
	
        res = (receive*)shmat(shmid, NULL, 0);
	res = res + sizeof(request);
	res->s_flag = 1;		
	while(res->s_flag);		// this infinite while shall be broken when the join_thread() 
					// has written the result back to the client and sets this flag as 0
	result = res->result;
	printf("%s : PID : %d got result : %d\n", __FILE__, getpid(), result);
	printf("\n%s : PID : %d Ended \n", __FILE__, getpid());
	
	printf("\n%s : PID : %d releasing semaphore\n", __FILE__, getpid());
        if(!sem_v(0))   /*Semaphore 1 released as value restored to sem_value, now available
                        for the server willing to write to the FIFO client_receive*/
                exit(EXIT_FAILURE);
	printf("\n%s : PID : %d semaphore realeased\n", __FILE__, getpid());
	
	return 0;
}
