#include"header.h"
#include"structures.h"
#include"declarations.h"

void* join_thread(void *arg) 		// join_thread begins
{
	int shmid, cret, jret, i = 0, count, result;
	static int clients = 1;
	printf("\n%s : Begin \n",__FILE__); 
	pthread_t tid;
	tid = *(pthread_t*)arg;		// tid to wait for
	receive *res, *res_value;
		
	printf("Waiting for fellow thread to exit..,\n");	
	jret = pthread_join(tid, (void**)&res_value);	//	wait for req_processing thread of this tid to end
	if (jret!= 0)
        {
	        perror("join()");
      	        exit(EXIT_FAILURE);
        }
	printf("Fellow thread exited !! Writing result to client..\n");	
	shmid = shmget((key_t) 1234, BUFSIZ, 0666 | IPC_CREAT);
       	if (shmid == -1)
        {
		perror("shmget()");
         	exit(EXIT_FAILURE);
        }	
	res = (receive*)shmat(shmid, NULL, 0);			// write that data
	res = res + sizeof(request);
	res->s_flag = 0;
	res->result = res_value->result;
	printf("result for %d : --> %d\n", res.pid, res->result);
	printf("\nThread wrote to the requesting client no : %d\n\v",clients);
	printf("\v############################################################\v\n");
	clients++;
	printf("\n%s : End \n",__FILE__); 
	pthread_exit(NULL);
}
