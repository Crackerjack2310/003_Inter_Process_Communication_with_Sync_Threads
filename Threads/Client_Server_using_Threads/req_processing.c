#include"header.h"
#include"structures.h"
#include"declarations.h"

void* req_processing(void* arg)
{
	printf("\n%s : Begin \n", __FILE__);
/*	if (!pthread_mutex_lock(&mutex))		// thread acquires mutex or waits
	{
		perror("pthread_mutex_lock()");
		exit("EXIT_FAILURE");
	}
	
	printf("thread acquired mutex\n");
*/		
	request *req;
	static receive res;	
	pid_t ret, client_pid;
	
	int oper, opr1, opr2, ptr, shmid;
        shmid = shmget((key_t) 1234, BUFSIZ, 0666 | IPC_CREAT);
        if (shmid == -1)
	{
		perror("shmget()");
		exit(EXIT_FAILURE);
	}
	req = (request*)shmat(shmid, NULL, 0);		// get a pointer to shared memory
	
	printf("Waiting for client to break the loop...\n\n");
	while(req->c_flag);		// stop till c_flag value not written to 0 by client	
	req->c_flag = 1;
	printf("loop broken\n");
	
//////////////////////////////////////////////////////////// read data from client

	opr1 = req->opr1;
	opr2 = req->opr2;
	client_pid = req->pid;
	oper = req->oper;

////////////////////////////////////////////////////////////	data read...now the shm can be used by other clients	
							//	set some flag to be read by client -> unlock mutex

	request *temp;
	temp = req + (sizeof(request) + sizeof(receive) + sizeof(request));
		
	temp->c_flag = 1;	// to bring procsessing client to wait in while;
	ret = fork();   // child process for req_processing for processing clients

        switch(ret)
	{        
                case -1:                         
                        perror("fork()");
                        exit(EXIT_FAILURE);
                case 0:                                 // child
			switch(oper)
			{
				case '+': execl("./adder","adder", NULL);        
				case '-': execl("./subtractor","subtractor", NULL);        
				case '*': execl("./multiplier","multipier", NULL);        
				case '/': execl("./divider","divider", NULL);        
				default:
					printf("Invalid operator choice....going on block !!\n");
			}
			break;
                default:                                // parent 
                        
			printf("This is req_processing unit\n");
			req = req + (sizeof(request) + sizeof(receive) + sizeof(request));
					
			req->opr1 = opr1;
			req->opr2 = opr2;
	
			printf("%s wrote to processing client \n",__func__);
			req->c_flag = 0;
       			waitpid(ret,&ptr,0);	// wait for proc. client to end 
			
			res.result = req->opr1;		// collect result
			res.pid = client_pid;		// collect PID of client
			printf("%s read from the requesting client\n",__func__);
	}

	printf("%s : says client PID : %d\n", __func__, client_pid);
/*	if (!pthread_mutex_unlock(&mutex)) 		//	release mutex
	{
		perror("pthread_mutex_unlock()");
		exit("EXIT_FAILURE");
	}
	printf("thread released mutex\n");
*/
	printf("\n%s : End \n", __FILE__);
	pthread_exit((void*)&res);		// sending the address 
}
