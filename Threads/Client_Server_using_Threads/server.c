#include"header.h"
#include"structures.h"
#include"declarations.h"

int main() 		// server begins
{
/*	if (pthread_mutex_init(&mutex, NULL))		//	initializing the mutex variable
	{
		perror("pthread_mutex_init()");
		exit(EXIT_FAILURE);
	}
*/	
	int sem_id = semget((key_t) 1234, 2, 0666 | IPC_CREAT);
        if(sem_id == -1)         // setting semaphore value
        {
                fprintf(stderr,"Failed to get sem_id\n");       // initializing semaphore, putting semval = val
                exit(EXIT_FAILURE);
        }

        if(!set_semvalue(0))     // setting semaphore value for using it the first time.
        {
                fprintf(stderr,"Failed to initialize semaphore at 0\n");// initializing semaphore, putting semval = val
                exit(EXIT_FAILURE);
        }

	system("clear");
        printf("got sem_id : %d\n",sem_id);
	signal(SIGINT, sig_handler); 		// for deleting the shared memory mad mutexes
	printf("\n%s : Begin \n",__FILE__); 
	
	int dret, i = 0;
	pthread_t tid[100000], drone;		// array for having various threads
	request *client;
        int shmid = shmget((key_t) 1234, BUFSIZ, 0666 | IPC_CREAT);
        if (shmid == -1)
        {
                perror("shmget()");
                exit(EXIT_FAILURE);
        }
        client = (request*)shmat(shmid, NULL, 0);
        
	while(1)		// infinite loop
	{
		client->drone_flag = 1;
        	printf("Drone searching for new clients...\n");
		while(client->drone_flag);	// loop breaks when client enters
		printf("Drone found a client !! \n\n");
        	dret = pthread_create(&tid[i], NULL, &drone_thread, NULL);
	        if (dret!= 0)
	        {
	                perror("Drone collapsed");
        	        exit(EXIT_FAILURE);
       	 	}
		i++;
	}
	return 0;
}

void sig_handler(int sig)  // as we need SIGINT to terminate server as of now. hence it deletes the mutex and shm as well.
{
	printf("\n%s : End \n",__FILE__);
       	int shmid = shmget((key_t) 1234, BUFSIZ, 0666 | IPC_CREAT);
       	if(shmctl(shmid, IPC_RMID, 0) == -1)
        {
	 	perror("shmctl()");
                exit(EXIT_FAILURE);
	}
	printf("Shared memory released successfully\n");

/*	if (pthread_mutex_destroy(&mutex))
	{
		perror("pthread_mutex_destroy()");
		exit(EXIT_FAILURE);
	}		
	printf("Mutex variable destroyed successfully\n");
*/	del_semvalue(0);
        printf("\nSemaphore at 0 deleted Successfully\n");
	exit(EXIT_SUCCESS);
}
