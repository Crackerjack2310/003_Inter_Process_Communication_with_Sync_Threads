#include"header.h"
#include"structures.h"
#include"declarations.h"

void* drone_thread(void *arg) 		// drone_thread begins
{
	printf("\n%s : Begin \n",__FILE__); 
	int cret, jret;
	pthread_t read, write;
	
/////////////////////////////////////////////////////////////////	create 2 threads

	cret = pthread_create(&read, NULL, &req_processing, NULL);
        //      create a thread to take input for the server    
        if (cret!= 0)
	{
                perror("pthread_create");
        	exit(EXIT_FAILURE);
        }

//////////////////////////////////////////////////////////////////      thread below shall wait for exit from above thread

        cret = pthread_create(&write, NULL, &join_thread, (void*)&read);
        //      this thread waits for the above thread to end
        if (cret!= 0)
        {
        	perror("pthread_create() for join_thread");
                exit(EXIT_FAILURE);
        }

/////////////////////////////////////////////////////////////////	parent thread waits for all child threads to end

	jret = pthread_join(write, NULL);   //      wait for req_processing thread of this tid to end
        if (jret!= 0)
        {
                perror("join() for write");
                exit(EXIT_FAILURE);
        }
	printf("\n%s : End \n",__FILE__); 
	pthread_exit(NULL);
}
