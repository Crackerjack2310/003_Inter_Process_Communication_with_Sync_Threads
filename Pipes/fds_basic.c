#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<string.h>
#include<fcntl.h>

void main()
{

	printf("\n%s : Begin  PID : %d   PPID: %d \n",__FILE__,getpid(),getppid());
	
	int arr[2],pret,rfd,wfd;

	pret=pipe(arr);
	if(pret==-1)
	{
		perror("pipe");
	}
	
	rfd=arr[0];
	wfd=arr[1];

	printf("\n rfd : %d   wfd : %d \n",rfd,wfd);

	printf("\n%s : End  PID : %d   PPID: %d \n",__FILE__,getpid(),getppid());
}


