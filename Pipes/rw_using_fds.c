#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<string.h>
#include<fcntl.h>

void main()
{

	printf("\n%s : Begin  PID : %d   PPID: %d \n",__FILE__,getpid(),getppid());
	
	int arr[2],pret,rfd,wfd,count;
	char *wbuff="The quieter u become, the more u r able to hear";
	char *rbuff;
	rbuff=(char*)malloc(sizeof(char)*strlen(wbuff));

	pret=pipe(arr);
	if(pret==-1)
	{
		perror("pipe");
		exit(EXIT_FAILURE);
	}
	
	rfd=arr[0];
	wfd=arr[1];
	
	count = write(wfd,wbuff,strlen(wbuff));
	
	printf("\nbytes written : %d\nText written : %s\n",count,wbuff);

	sleep(2);

	read(rfd,rbuff,count);
	
	printf("\nbytes read : %d\nText read : %s\n",(int)strlen(wbuff),rbuff);

	printf("\n%s : End  PID : %d   PPID: %d \n",__FILE__,getpid(),getppid());
}


