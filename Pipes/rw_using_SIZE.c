#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<string.h>
#include<fcntl.h>
#include<sys/wait.h>
#define SIZE 1024*2

void main()
{

	printf("\n%s : Begin  PID : %d   PPID: %d \n",__FILE__,getpid(),getppid());
	
	int arr[2],pret,rfd,wfd,count,w,s=0,ret=0;
	pid_t fret;
	char *wbuff="The quieter u become, the more u r able to hear";
	char *rbuff,*srfd,*scount;
	rbuff=(char*)malloc(sizeof(char)*strlen(wbuff));
	srfd=(char*)malloc(sizeof(int));
	scount=(char*)malloc(sizeof(int));

	pret=pipe(arr);
	if(pret==-1)
	{
		perror("pipe");
		exit(EXIT_FAILURE);
	}
	
	rfd=arr[0];
	wfd=arr[1];
	
	fret = fork();
		
	switch(fret)
	{
		case -1:
			perror("fork");
			exit(EXIT_FAILURE);
		
		case 0:		//child only
			sleep(2);
			sprintf(srfd,"%d",rfd);
			sprintf(scount,"%d",(int)strlen(wbuff));
			execl("./new_for_SIZE","new_for_SIZE",srfd,scount,NULL);	
			break;
		default:		//Parent only
			while(SIZE>s)
			{
				ret = write(wfd,wbuff,1);		//copy on write
				s++;
			        printf("written %d bytes \n",s);
			}
			printf("\nParent done waiting for child\n");
		//	printf("count : %d \n",count);
		//	printf("\nbytes written : %d\nText written : %s\n",count,wbuff);
			wait(&w);	
	}
	printf("\n%s : End  PID : %d   PPID: %d \n",__FILE__,getpid(),getppid());
}


