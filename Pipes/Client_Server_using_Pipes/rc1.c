#include"request.h"
#include"header.h"
#include"declarations.h"

int main(int argc, char *argv[]) 	// client 1
{
	printf("\n%s : Begin -- client1 --\n",__FILE__); 
	
	int rfd,wfd, count, a, b;
	request r;
	wfd = atoi(argv[1]);	//write file descriptor of pipe 1
	rfd = atoi(argv[2]);	//read file descriptor of pipe 3
	
	printf("wfd value : %d\n",wfd);
	printf("rfd value : %d\n",rfd);
	printf("Enter data for client as operator, oper1 and oper2 \n");
	scanf("%c%d%d",&r.oper,&r.opr1,&r.opr2);
	
	printf("Entered data : operator : %c operand 1 : %d  operand 2 : %d \n",r.oper,r.opr1,r.opr2);

	count = write(wfd,&r,sizeof(request));	// write the structure to the pipe 1
	printf("\n%s wrote %d bytes into pipe 1\n",__FILE__,count); 
	
	count = read(rfd,&r,sizeof(request));	// write the structure to the pipe 3
	printf("\n%s read %d bytes from pipe 3\n",__FILE__,count);
        
	printf("value after processing -->>\n r.result : %d \n",r.result);

	printf("\n%s : End -- client1 --\n",__FILE__);
	return 0;

}
