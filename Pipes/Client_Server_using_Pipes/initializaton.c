#include"header.h"
#include"request.h"
#include"declarations.h"

int* initialization(int noc)
{
	printf("\n%s : Begin \n",__func__); 

	int *fds,i,pret,limit = 6;
	fds = (int*)malloc(sizeof(int)*((noc*2) + 2 + 2 ));  	// no of file descriptors required for this noc value
		// 1 pipe for each client 1 pipe for adder, 
	
	for(i = 0; i < limit ; i = i + 2)               // limit is the no of file descriptors needed
        {
                pret = pipe(fds + i);

                if (pret == -1)
                { 
                        perror("pipe ()");
                        exit(EXIT_FAILURE);
                }
        }                                       // creating the required pipes

	printf("\n%s : End \n",__func__);
	return fds;
}
