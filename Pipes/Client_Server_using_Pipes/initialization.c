#include"header.h"
#include"request.h"
#include"declarations.h"

int* initialization(int noc)
{
	printf("\n%s : Begin \n",__func__); 

	int *fds,i,pret,limit = (noc*2 + 4 + 2 );

	fds = (int*)malloc(sizeof(int)*limit);  	// no of file descriptors required for this noc value
		// 2 --- for comm. with client for data input  		 3 4
		// 2 --- common pipe for result writing to client  	 5 6
		// 2 --- for comm. with processing client sending        		 7 8
		// 2 --- for comm. with processing client receiving	      		 9 10

	for(i = 0; i < limit ; i = i + 2)               // limit is the no of file descriptors needed
        {
                pret = pipe(fds + i);

                if (pret == -1)
                { 
                        perror("pipe ()");
                        exit(EXIT_FAILURE);
                }
        }                                       // creating the required pipes

	printf("fds created. no of fds : %d \n",i);
	printf("array is : %d \n",i);

	for(i = 0; i < limit ; i++)               // limit is the no of file descriptors needed
	{
		printf("array value at %d : %d \n",i, *(fds + i));
	}

	printf("\n%s : End \n",__func__);
	return fds;
}
