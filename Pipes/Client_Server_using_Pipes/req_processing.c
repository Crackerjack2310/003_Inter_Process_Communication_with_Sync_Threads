#include"header.h"
#include"request.h"
#include"declarations.h"


request req_processing(int *fds)
{
	printf("\n%s : Begin \n",__func__); 
	
	char *swfd,*srfd;
	swfd = (char*)malloc(sizeof(int));
	srfd = (char*)malloc(sizeof(int));

	request r;
	int ret, count, rfd, wfd;
	
	count = read(*fds,&r,sizeof(request));		// read the structure written to pipe1 by client 
	printf("%s read %d bytes from pipe 1\n",__func__,count);

	ret = fork();   // child process for req_processing for processing clients

        switch(ret)
        {
                case -1:                         
                        perror("fork()");
                        exit(EXIT_FAILURE);
                case 0:                                 // child
                        sprintf(swfd,"%d",*(fds + 5));  // sending the wfd to process. client for wrting to the pipe 3
                        sprintf(srfd,"%d",*(fds + 2));  // sending the rfd to process. client for reading from the pipe 2
            		
			switch(r.oper)
			{
				
				case '+': execl("./adder","adder",srfd,swfd,NULL);        
				case '-': execl("./subtractor","subtractor",srfd,swfd,NULL);        
				case '*': execl("./multiplier","multipier",srfd,swfd,NULL);        
				case '/': execl("./divider","divider",srfd,swfd,NULL);        
				default:
					  printf("Invalid operator choice\n");
					  exit(EXIT_FAILURE);
			}

                default:                                // parent 
                        printf("This is req_processing unit\n");
			count = write(*(fds + 3),&r,sizeof(request));	// write the structure to pipe2 for adder
			count = read(*(fds + 4),&r,sizeof(request));	// read the structure written to pipe3 by adder 
			printf("%s read %d bytes from pipe 2\n",__func__,count);
        		
	}

	printf("\n%s : End \n",__func__);
	return r;
}
