#include"header.h"
#include"request.h"
#include"declarations.h"

int main() // server begins
{
	printf("\n%s : Begin \n",__func__); 

	int noc, *fds, count, ptr, wt;
	pid_t ret;
	printf("Enter number of clients : \n");
	scanf("%d",&noc);
	printf("ok %d clients : \n",noc);
	
	char *srfd,*swfd;
	swfd = (char*)malloc(sizeof(int));
	srfd = (char*)malloc(sizeof(int));
	request r;

	fds = initialization(noc); 		// get the integer array corresponding to no. of clients
	
	ret = fork();		// child of server created for client 1

	switch(ret)
	{
		case -1:
			perror("fork()");
			exit(EXIT_FAILURE);
		case 0:					// child
			sprintf(swfd,"%d",*(fds + 1));	// sending the wfd to rc1 for wrting to the pipe1
			sprintf(srfd,"%d",*(fds + 6));	// sending the rfd to rc1 for reading from pipe4
			execl("./rc1","rc1",swfd,srfd,NULL);	// child 1 dies		
			break;
		default:				// server
			printf("This is SERVER .. waiting for req_processing\n");
			r = req_processing(fds);
			printf("Got the calculated values from req_processing\n");
			printf("This is SERVER, ready to write to client for result\n");
			count = write(*(fds + 7),&r,sizeof(r)); // writing to pipe4 by server
			printf("Server wrote %d bytes to the request client1\n",count);
			waitpid(ret,&ptr,0);	
			
		
	}	

	printf("\n%s : End \n",__func__);
	return 0;
}
