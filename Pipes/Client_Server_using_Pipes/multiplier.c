#include"header.h"
#include"request.h"
#include"declarations.h"

int main(int argc, char *argv[]) // multiplier
{
	printf("\n%s : Begin -- multiplier -- \n",__func__); 

	int wfd, rfd, count;
	request r;

	wfd = atoi(argv[2]);	// this is wfd for pipe 3 that is fd = 8
	rfd = atoi(argv[1]);	// this is rfd for pipr 2 that is fd = 5
	count = read(rfd,&r,sizeof(r)); // reading the structure from pipe 2
//	printf("value read by %s from pipe 2 : %d bytes\n",__FILE__,count);
	r.result = r.opr1 * r.opr2; // adding and writing value to result

	count = write(wfd,&r,sizeof(r)); // writing the calculated value into pipe 3
// 	printf("value written by %s to pipe 3 : %d bytes\n",__FILE__,count);
	printf("\n%s : End -- multiplier -- \n",__func__);
	
	return 0;
}
