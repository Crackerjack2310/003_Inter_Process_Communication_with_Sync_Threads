#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<string.h>
#include<fcntl.h>
#include<sys/wait.h>

void main()
{

	printf("\n%s : Begin  PID : %d   PPID: %d \n",__FILE__,getpid(),getppid());
	
	int arr[2],pret,rfd,wfd,count,w;
	pid_t fret;
	char *wbuff="The quieter u become, the more u r able to hear";
	char *rbuff;
	rbuff=(char*)malloc(sizeof(char)*strlen(wbuff));

	pret=pipe(arr);
	if(pret==-1)
	{
		perror("pipe");
		exit(EXIT_FAILURE);
	}
	
	rfd=arr[0];
	wfd=arr[1];
	
	fret = fork();
		
	switch(fret)
	{
		case -1:
			perror("fork");
			exit(EXIT_FAILURE);
		
		case 0:		//child only
			sleep(2);
			printf("rfd : %d  count : %d \n",rfd,count);
			read(rfd,rbuff,(int)strlen(wbuff));
			printf("\nbytes read : %d\nText read : %s\n",(int)strlen(wbuff),rbuff);
			break;
		default:		//Parent only
			count = write(wfd,wbuff,strlen(wbuff));		//copy on write
	
			printf("count : %d \n",count);
			printf("\nbytes written : %d\nText written : %s\n",count,wbuff);
			wait(&w);	
	}
	printf("\n%s : End  PID : %d   PPID: %d \n",__FILE__,getpid(),getppid());
}


